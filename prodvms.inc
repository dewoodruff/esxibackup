#!/bin/bash

# This file will be included in the ESXi backup script.
# It specifies the VMs that should be backed up.

# Set the report name to be use in the email subject
REPORTNAME="Production VMs"

# Older versions of bash don't have hashes... sigh.
# So let's create two arrays to define VM names and their respective datastores
vms[0]="My_VM1"
datastores[0]="VM_prod"
vms[1]="My_VM2"
datastores[1]="VM_prod"
vms[2]="Test1"
datastores[2]="VM_test"

# add as many pairs as needed
