#!/bin/bash
#
# Author: Dan Woodruff
# Oct 2011
#
# Description:  Simple BASH script that does the following:
#		Specify VMs for backup
#               Snapshots running VMs
#               Copies VMs to local storage using SCP
#               Removes snapshots of running VMs
#               Outputs result to file and sends an email
#
# Requirements: VM name must be the same as the VM directory name
#
#		Must have SSH authentication keys setup between
#               server running this script and the ESXi host.
#
#               SSH Authorization Setup
#               On server orchestrating the backup, run these commands as the user who will do the backups. If scheduled via cron, this is likely root:
#		1. ssh-keygen -t rsa
#		2. scp ~/.ssh/id_rsa.pub root@esxserver:/authorized_keys
#		On ESXi server for ESXi v4 and below
#		1. mkdir /.ssh
#		2. mv /authorized_keys /.ssh
#		3. cp -r /.ssh /vmfs/volumes/datastore1/  ### replace 'datastore1' with the name of your datastore
#               4. Add following line at the end of /etc/rc.local so that the key will be replaced on reboot,
#			replacing 'datastore1' with the name of your datastore: 
#		cp /vmfs/volumes/datastore1/.ssh /.ssh -R
#		
#		For ESXi v5
#		1. cat /authorized_keys > /etc/ssh/keys-root/authorized_keys
#		That's it. authorized_keys is persistent by default in vsphere 5
#		###############
#
#		Validate you can SSH to the ESXi server without entering a password.
#
# Restore:      Copy backup directory files to ESXi datastore
#               Browse datastore, Add the vmx file to Inventory
#               Start the VM
#
# Usage:        esxi_backup.sh file_to_include_with_vm_specifications
#
#####################################################################################################

# Environment variables
HOSTNAME=marie.penthouse.pad                  # Hostname or IP address of your ESXi server
BASE_DIR=/mir/Backup_ESXi              # Base directory where backups will go
LOG_DIR=$BASE_DIR/logs                        # Location of log directory
DATE=`date +%F`                               # Date format for log filename
LOG=$LOG_DIR/$DATE.log                        # Name of log file
MAILCMD="mail"
MAILTO="daniel.woodruff@gmail.com"            # Email of user(s) to receive email report (use comma to seperate addresses)
SNAP_NAME=$DATE-BACKUP
RETENTION=60				      # Archives older than this # of days are deleted.
# Include the file specified on cmd line with VM defs.
[ -f "$1" ] || { echo "Usage: $0 file_to_include_with_vm_specifications"; exit 0; }
. "$1"


# Start time of script
SCRIPT_START=`date +%s`

total=${#vms[*]}

# Create a new directory if it doesn't already exist
if [ -d $BASE_DIR ] # directory exists
then
	EXISTS=true
else
	mkdir $BASE_DIR
fi

# Create a new directory for the log if it doesn't already exist
if [ -d $LOG_DIR ] # directory exists
then
	EXISTS=true
else
	mkdir $LOG_DIR
fi

echo "............................" | tee -a $LOG
echo "ESXi Backup [ $HOSTNAME ] `date`" | tee -a $LOG
echo "............................" | tee -a $LOG
echo "Keeping $RETENTION days of archives" | tee -a $LOG

# For each VM defined above
for (( i=0; i<=$(( $total -1 )); i++ )); do
	SNAP_TAKEN=0
	PREVIOUS_SNAP=0
	STORE=${datastores[$i]}
	VM_NAME=${vms[$i]}
	VM_DIR="/vmfs/volumes/$STORE/$VM_NAME"
	BACKUP_DIR="$BASE_DIR/$VM_NAME-$DATE"
	VMX=$( ssh $HOSTNAME "find '$VM_DIR' -name *.vmx 2>&1" ) 2>>$LOG
	if [[ $VMX == *"No such file or directory"* ]]; then
		echo "`date` - $VM_DIR not found! Make sure your host specification is correct." | tee -a $LOG
		continue
	fi
	VM_START=`date +%s`
	
	# Delete old archives according to retention specifications.
	echo "`date` - Deleting old archives for $VM_NAME > $RETENTION days old..." | tee -a $LOG
	find "$BASE_DIR" -maxdepth 1 -name "$VM_NAME*" -mtime +"$RETENTION" -exec rm -rf {} \; 2>>$LOG | tee -a $LOG
	echo "`date` - Done." | tee -a $LOG

	# Create a new directory for the backup if it doesn't already exist.
	# If it does, print a message and move on to the next VM
	if [ -d $BACKUP_DIR ] # directory exists
	then
		echo "`date` - $BACKUP_DIR already exists. Has $VM_NAME already been backed up today?" | tee -a $LOG
		continue
	else
		mkdir $BACKUP_DIR
	fi

	# Copy vmx file before snapshot is taken
	echo "`date` - Staring backup of: $VM_NAME from $STORE." | tee -a $LOG
	echo "`date` - Copying $VMX from $STORE." | tee -a $LOG
	scp $HOSTNAME:"'$VMX'" "$BACKUP_DIR" 2>>$LOG
	
	# check if previous snapshots exist and copy the database if they do
	if [ "$(ssh $HOSTNAME "vim-cmd vmsvc/snapshot.get '$VMX'")" != "Get Snapshot:" ]; then
		echo "`date` - There are previous snapshots for $VM_NAME - including snapshot database." | tee -a $LOG
		PREVIOUS_SNAP=1
		scp $HOSTNAME:"$VM_DIR"/*.vmsd "$BACKUP_DIR" 2>>$LOG
	fi
		
	# If VM is running create a snapshot with the quiesce option
	if [ "$(ssh $HOSTNAME "vim-cmd vmsvc/power.getstate '$VMX' | grep Powered")" == "Powered on" ]; then
		echo "`date` - VM is on, creating snapshot." | tee -a $LOG
		SNAP_TAKEN=1
		ssh $HOSTNAME "vim-cmd vmsvc/snapshot.create '$VMX' $SNAP_NAME esxibackup.sh_$DATE 0 1" 2>>$LOG
	fi
	
	# Copy vmdk files
	echo "`date` - Copying virtual disk files" | tee -a $LOG
	scp $HOSTNAME:"$VM_DIR"/*.vmdk "$BACKUP_DIR" 2>>$LOG

	# Remove snapshots if any were taken
	if [ $SNAP_TAKEN == 1 ]; then
		echo "`date` - Removing the snapshot that was taken." | tee -a $LOG
		# As an extra level of safety, make sure there actually is a snapshot with the expected name.
		# Wouldn't want the backup script to remove an important snap.
		S="$(ssh $HOSTNAME "vim-cmd vmsvc/snapshot.get '$VMX'" | grep -A1 "$SNAP_NAME" )"
		if [[ $S == *"$SNAP_NAME"* ]]; then
			SNAP_ID=`echo $S | grep -oP 'Snapshot Id : [0-9]+' | cut -f4 -d\ `
			#if [ $PREVIOUS_SNAP == 1 ]; then
			#	echo "`date` - Only removing the last snapshot, since others exist." | tee -a $LOG
			#	NUM_SNAP="$(ssh $HOSTNAME "vim-cmd vmsvc/snapshot.get '$VMX'" | grep "Snapshot Name" | wc -l)"
			#	NUM_SNAP=$(( NUM_SNAP - 1 ))
			#	ssh $HOSTNAME "vim-cmd vmsvc/snapshot.remove '$VMX' 0 $NUM_SNAP 0" 1>/dev/null 2>>$LOG
			#else
			#	echo "`date` - Removing the snapshot." | tee -a $LOG
			#	ssh $HOSTNAME "vim-cmd vmsvc/snapshot.remove '$VMX' " 1>/dev/null 2>>$LOG
			#fi
			#echo "`date` - Removing the snapshot." | tee -a $LOG
			ssh $HOSTNAME "vim-cmd vmsvc/snapshot.remove '$VMX' '$SNAP_ID' " 1>/dev/null 2>>$LOG
		else
			echo "`date` - There was a problem finding the snapshot that was supposedly created for this backup. Please check the snapshots for this VM." | tee -a $LOG
		fi
	fi	

	VM_END=`date +%s`
	echo "`date` - Completed backup of $VM_NAME in $(( VM_END - VM_START)) seconds." | tee -a $LOG
	echo "............................" | tee -a $LOG
done

# Print daily backup directory listing
echo "Backup utilization" | tee -a $LOG
du -sh $BASE_DIR | tee -a $LOG
df -h $BASE_DIR | tee -a $LOG
zfs list $BASE_DIR | tee -a $LOG
zpool list mir | tee -a $LOG


# End time of script
SCRIPT_END=`date +%s`

echo "All backups completed in $(( SCRIPT_END - SCRIPT_START)) seconds." | tee -a $LOG

#Statement to mail log file to appropriate users
$MAILCMD -s "ESXi Backup Report for $HOSTNAME - $REPORTNAME" $MAILTO < $LOG

exit
